import { Deserializable } from './deserializable.model';
import { JwtToken } from './jwtToken.model';
import { Md5 } from 'ts-md5/dist/md5';

export class User implements Deserializable {
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    avatarURL?: string;
    token?: JwtToken;

    deserialize(input: any): this {
        Object.assign(this, input);
        this.token = new JwtToken().deserialize(input.token);
        
        if(!this.avatarURL){
        const emailHash = Md5.hashStr(this.email.trim().toLowerCase());
        this.avatarURL = 'https://www.gravatar.com/avatar/'+ emailHash +'?s=404&d=wavatar';
        
          }
                
        return this;
    }
      
     fullName() {
        return this.firstName + ' ' + this.lastName;
    }
}
